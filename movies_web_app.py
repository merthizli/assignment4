#  No module named MySQLdb
# mysql db is only for python2 

#neden son imagei almiyor da hala ilk image i almaya calisiyor ?
# error handling - (olmayan bisi delete etmek? )

import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application

def sql_connector():
    cnx = ''
    db, username, password, hostname = get_db_creds()
    try:
        cnx = mysql.connector.connect(user=username,
                                      host=hostname,
                                      password=password,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, db=db, password=password)

    return cnx

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None) 
    username = os.environ.get("USER", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None) 
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None) 
    return db, username, password, hostname

@app.route('/create_db')
def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movie_db (id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating FLOAT, PRIMARY KEY (id))'

    cnx = sql_connector()
    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        #populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
            return  str(err)
        else:
            print(err.msg)
            return str(err)
    return 'OK'


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Hostname: %s" % hostname)

    cnx = sql_connector()
    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


from functools import wraps




@app.route('/search_movie', methods=['GET'])
def query_data():
    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Hostname: %s" % hostname)


    cnx = sql_connector()
    cur = cnx.cursor()
    #return (str(request.forms))
    #return (str(request.args.to_dict()))
    cur.execute("SELECT title,year,actor FROM movie_db where upper(actor)=" + '\'' + request.args.to_dict()['search_actor'].upper()+ '\'')   
    #entries = [row for row in cur.fetchall()]

    entries = [ "<xmp>" + str(map(str,row)) + "</xmp>"  for row in cur.fetchall()]
    if not entries:
        return 'No movies found for actor %s' % request.args.to_dict()['search_actor']
    result = "<xmp>" + "title,year,actor" + "</xmp>"
    for entry in entries:
        result += entry
    return result


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    msg = request.form.to_dict()
    db, username, password, hostname = get_db_creds()
    cnx = sql_connector()
    cur = cnx.cursor()
    sql = "select * from movie_db where upper(title) = '" + msg['delete_title'].upper() + '\''
    cur.execute(sql)
    res = cur.fetchall()
    if not res:
        return "Movie %s does not exist." % msg['delete_title']
    #return (str(msg))
    sql= "DELETE FROM movie_db where title=" + '\'' + msg['delete_title'] + '\''
    try:
        cur.execute (sql)
    except Exception as e:
        return "An Error occured while trying to delete the movie: \n\n " + str(e)
    cnx.commit()
    return 'Movie %s successfully deleted' % msg['delete_title']


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    msg = request.form.to_dict()
    db, username, password, hostname = get_db_creds()

    cnx = sql_connector()
    cur = cnx.cursor()

    sql = "select * from movie_db where upper(title) = '" + msg['title'].upper() + '\''
    cur.execute(sql)
    res = cur.fetchall()
    if res:
        return 'Cant add this movie, because a movie with a same name already exists.'

    sql = "INSERT INTO movie_db (year,title,director,actor,release_date,rating) VALUES (%s, %s,%s, %s, %s, %s)"
    val = (msg['year'], msg['title'],msg['director'],msg['actor'],msg['release_date'],msg['rating'])
    print(val)
    try:
        cur.execute(sql, val)
    except Exception as e:
        return "Error occured while trying to add a movie: \n\n" + str(e)
    cnx.commit()
    return 'Movie %s is succesfully inserted' % msg['title']

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    msg = request.form.to_dict()
    db, username, password, hostname = get_db_creds()
    cnx = sql_connector()
    cur = cnx.cursor()
    sql = "Select * from movie_db where upper(title)=" + '\'' +  request.form.to_dict()['title'].upper() + '\''
    cur.execute(sql)
    res = cur.fetchall()
    if res:
        # sql = "UPDATE movie_db SET year='%s', director='%s', actor='%s', release_date='%s', rating='%s' where title='%s'" %  (request.form.to_dict()['year'],request.form.to_dict()['director'], request.form.to_dict()['actor'], request.form.to_dict()['release_date'],request.form.to_dict()['rating'], request.form.to_dict()['title']  ) 
        sql = "UPDATE movie_db SET year='%s', director='%s', actor='%s', release_date='%s', rating='%s' where title='%s'" %  (request.form.to_dict()['year'],request.form.to_dict()['director'], request.form.to_dict()['actor'], request.form.to_dict()['release_date'],request.form.to_dict()['rating'], request.form.to_dict()['title']  )
    else:
        return "The movie with a given name does not exist. "
    #check if the name exits:
        #update
    #if dne 
        #create or fail
    #sql = "INSERT INTO movie_db (year,title,director,actor,release_date,rating) VALUES (%s, %s,%s, %s, %s, %s)"
    #val= tuple(msg.values())
    try:
        cur.execute(sql)
    except Exception as e:
        return "An error occured while trying to update the movie: \n\n" + str(e)     
    cnx.commit()
    return 'Movie %s is succesfully updated' % msg['title']



@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    cnx = sql_connector()
    cur = cnx.cursor()
    sql = "SELECT title FROM movie_db"
    cur.execute("SELECT * FROM movie_db f  where not exists (select * from movie_db s where s.rating>f.rating)")
    res = cur.fetchall()
    if not res:
        return 'There is no movies in the database yet.'
    res = ["<xmp>" + str(map(str,row[1:])) + "</xmp>" for row in res]
    result = "<xmp>" + "year, title, director, actor,release_date,rating" + "</xmp>"
    for entry in res:
        result += entry
    return result

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    cnx = sql_connector()
    cur = cnx.cursor()
    cur.execute("SELECT * FROM movie_db f  where not exists (select * from movie_db s where s.rating < f.rating)")
    res = cur.fetchall()
    if not res:
        return 'There is no movies in the database yet.'
    res = ["<xmp>" + str(map(str,row[1:])) + "</xmp>" for row in res]
    result = ''
    result = "<xmp>" + "year, title, director, actor,release_date,rating" + "</xmp>"
    for entry in res:
        result += entry
    return result

@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
